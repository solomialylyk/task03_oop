package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;
import com.epam.model.Goods;
import com.epam.model.LogicAndFilling;
import com.epam.model.Manager;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MyView extends LogicAndFilling implements Printable {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - found by name Washbasin");
        menu.put("2", "  2 - read unsorted list");
        menu.put("3", "  3 - found Washbsins wuth price < 3500");
        menu.put("4", "  4 - found by name Door");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1() {
       // Manager t = new Manager();
        System.out.println("\nFound by name");
        List<Goods> res_name = controller.findByGroup("washbasin", tov);
        print(res_name);

    }

    private void pressButton2() {
    System.out.println("Unsorted");
        for (int i = 0; i < tov.size(); i++)
            System.out.print(tov.get(i));

    }

    private void pressButton3() {
        //Manager t = new Manager();
        System.out.println("\nSorted by prize");
        List<Goods> res_name = controller.findByGroup("washbasin", tov);
        List<Goods> res = controller.SortByPrize(3500, res_name);
        print(res);

    }


    private void pressButton4() {
        //Manager t = new Manager();
        System.out.println("\nFound by name");
        List<Goods> res_name = controller.findByGroup("door", tov);
        print(res_name);

    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).printGoods();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }


    @Override
    public void printGoods() {

    }
    private static void print(List<Goods> good) {
        System.out.println(good);
    }
}
