package com.epam.controller;

import com.epam.model.Goods;
import com.epam.model.LogicAndFilling;
import com.epam.model.Manager;
import com.epam.model.Model;

import java.util.List;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new Manager();
    }

    @Override
    public List<Goods> findByGroup(String name, List<Goods> tov) {
        return model.findByGroup( name,  tov);
    }

    @Override
    public List<Goods> SortByPrize(int amount, List<Goods> tov) {
        return model.SortByPrize(amount, tov);
    }
}
