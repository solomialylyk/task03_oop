package com.epam.model;

public class WoodenProduct extends Goods {
    String typeOfTree;

    public WoodenProduct(String name, String color, int prize, String typeOfTree) {
        super(name, color, prize);
        this.typeOfTree = typeOfTree;
    }

    @Override
    public String toString() {
        return "WoodenProduct{" +
                " name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", prize=" + prize +
                ", typeOfTree='" + typeOfTree + '\'' +
                '}'+ '\n';
    }
}
