package com.epam.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public interface Model {
    List<Goods> findByGroup(String name, List<Goods> tov);


    List<Goods> SortByPrize(int amount, List<Goods> tov);
}
