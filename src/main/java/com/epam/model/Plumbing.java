package com.epam.model;

public class Plumbing extends Goods{
    String material;

    public Plumbing(String name, String color, int prize, String material) {
        super(name, color, prize);
        this.material = material;
    }



    @Override
    public String toString() {
        return "Plumbing{" +
                " name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", prize=" + prize +
                ", material='" + material +
                '}'+ '\n';
    }
}
